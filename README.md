### Install ansible

#### debian based distro
```bash
sudo apt update && \
sudo apt install software-properties-common && \
sudo apt-add-repository --yes --update ppa:ansible/ansible && \
sudo apt install ansible
```

### Run playbook
```bash
ansible-playbook -i inventory main.yml --ask-become-pass
```
or
```bash
./ai --tags node
```